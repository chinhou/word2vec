cd src
make
cd ..

time ./bin/word2vec -train /work/data/NLP/corpora/raw_texts/Eng/Wikipedia/EnWiki.2017.05/AA_part/100M/corpus.txt -output ./embeddings/vectors.bin -cbow 0 -size 500 -window 5 -negative 1 -hs 0 -sample 0 -threads 32 -binary 1